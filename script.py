#! /usr/bin/env nix-shell
#! nix-shell -i python

import asyncio
import json
import logging

from asyncio_mqtt import Client
import websockets

logging.basicConfig(level=logging.INFO)

URI = "wss://cof.ens.fr/ws/k-fet/open/"
MQTT_HOST = "new.hackens.org"


is_open = None

async def main():
    async with Client(MQTT_HOST) as client:
        async with websockets.connect(URI) as ws:
            logging.info("Connected websocket and MQTT broker")
            await asyncio.gather(watch_for_changes(client, ws))

async def watch_for_changes(client, ws):
    global is_open
    async for msg in ws:
        logging.debug(f"Received following websocket message: '{msg}'")
        if json.loads(msg)['status'] == "opened":
            is_open = '1'
            led_info = '{ "ledKF": true }'
        else:
            is_open = '0'
            led_info = '{ "ledKF": false }'
        await send(client, "kfet/open", is_open)
        await send(client, "hackens/leds", led_info)

async def send(client, topic, msg):
    await client.publish(topic, payload=msg.encode(), retain=True)
    logging.debug(f"Sent '{is_open}' to MQTT topic {topic}")

asyncio.run(main())
