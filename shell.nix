with import <nixpkgs> {};
mkShell {
  packages = [
    (python39.withPackages (ps: [
      ps.asyncio-mqtt
      ps.websockets
    ]))
  ];
}
